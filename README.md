# Q&A Application (QuAndA)

An application developed in context of a Staffbase
bootcamp. Users can ask questions which others can
answer.

## User Workflow

A user can post a question, which has a *title* and a *description*.
Others can answer to that question also with a title and a description.
Both questions and answers can be commented.
The description text can be in markdown syntax.