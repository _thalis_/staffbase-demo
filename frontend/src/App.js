import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import TodoAppBar from "./components/QuandaAppBar";

class App extends Component {

    state = {};

    _interval;

    componentDidMount() {
        this._interval = setInterval(this.hello, 250);
    }

    componentWillUnmount() {
        clearInterval(this._interval);
    }

    hello = () => {
        fetch('/api/hello')
            .then(response => response.text())
            .then(message => {
                this.setState({message: message});
            });
    };

    render() {
        return (
            <div className="App">
                <TodoAppBar/>
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1>{this.state.message}</h1>
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </div>
        );
    }
}

export default App;
