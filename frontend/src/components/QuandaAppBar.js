import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {Typography} from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';
import {ToolBarNav} from './ToolBarNav';
import {Link as RouterLink} from 'react-router-dom';
import Link from '@material-ui/core/Link';
import {ClipboardListOutline} from 'mdi-material-ui';
import SvgIcon from '@material-ui/core/SvgIcon';

const styles = makeStyles(theme => ({
    appBar: {
        borderBottom: `1px solid ${ theme.palette.divider }`
    },
    toolBar: {
        flexWrap: 'wrap'
    },
    appIcon: {
        margin: theme.spacing(1, 1.5),
        width: 60,
        height: 60
    },
    toolBarTitle: {
        flexGrow: 1
    }
}));

const WelcomeLink = React.forwardRef((props, ref) =>
    <RouterLink innerRef={ ref }
                to="/" { ...props } />
);

export default function TodoAppBar() {
    const classes = styles();
    return (
        <AppBar position="static"
                color="default"
                elevation={ 0 }
                className={ classes.appBar }
        >
            <Toolbar className={ classes.toolBar }>
                <SvgIcon aria-hidden="true"
                         color="disabled"
                         className={ classes.appIcon }
                >
                    <ClipboardListOutline/>
                </SvgIcon>
                <Typography variant="h6"
                            color="inherit"
                            noWrap
                            className={ classes.toolBarTitle }
                >
                    <Link component={ WelcomeLink }
                          color="inherit"
                          underline="none"
                    >
                        QuAndA
                    </Link>
                </Typography>
                <ToolBarNav/>
            </Toolbar>
        </AppBar>
    )
}