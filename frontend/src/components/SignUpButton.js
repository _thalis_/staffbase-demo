import Button from '@material-ui/core/Button';
import React from 'react';
import { Link } from 'react-router-dom';

const SignUpLink = React.forwardRef((props, ref) => (
    <Link innerRef={ ref }
          to="/register" { ...props }/>
));

export default function SignUpButton(props) {
    return (
        <Button variant="outlined"
                color="default"
                href="#"
                component={ SignUpLink }
                { ...props }
        >
            Sign Up
        </Button>
    );
}