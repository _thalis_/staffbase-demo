import React from 'react';
import {Container, TextField, Typography} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
    signUpPanel: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    signUpForm: {
        marginTop: theme.spacing(3)
    },
    infoText: {
        margin: theme.spacing(3, 0, 2)
    },
    submitButton: {
        margin: theme.spacing(3, 0, 2)
    }
});

class SignUpPanel extends React.Component {

    initialState = {
        signUpSuccessful: false,
        submitError: '',
        emailField: {
            value: '',
            helperText: '',
            error: false
        },
        confirmEmailField: {
            value: '',
            helperText: '',
            error: false
        }
    };

    constructor(props) {
        super(props);
        this.state = Object.assign({}, this.initialState);

        this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this);
        this.handleConfirmedEmailAddressChange = this.handleConfirmedEmailAddressChange.bind(this);
        this.tryToRegister = this.tryToRegister.bind(this);
    }

    handleEmailAddressChange(event) {
        const newValue = event.target.value;
        this.setState(prevState => {
            const newState = Object.assign({}, prevState);
            newState.emailField.value = newValue;
            return newState;
        });
    }

    handleConfirmedEmailAddressChange(event) {
        const newValue = event.target.value;
        this.setState(prevState => {
            const newState = Object.assign({}, prevState);
            newState.confirmEmailField.value = newValue;
            return newState;
        });
    }

    tryToRegister(event) {
        if (this.state.emailField.value !== this.state.confirmEmailField.value) {
            this.setState(prevState => {
                const newState = Object.assign({}, prevState);
                newState.emailField.error = true;
                newState.emailField.helperText = 'The mail address must be equal to the confirmed one';
                return newState;
            });
        } else {
            const mailAddress = this.state.emailField.value;
            const confirmedMailAddress = this.state.confirmEmailField.value;
            fetch('/api/user/register', {
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                body: `mailAddress=${mailAddress}&repeatedMailAddress=${confirmedMailAddress}`
            })
                .then(response => {
                    if (!response.ok) {
                        this.setState(prevState => {
                            const newState = Object.assign({}, prevState);
                            newState.submitError = 'An error occurred trying to sign up. Please try again later.';
                            return newState;
                        });
                    } else {
                        this.setState(() => {
                            const newState = Object.assign({}, this.initialState);
                            newState.signUpSuccessful = true;
                            return newState;
                        });
                    }
                });
        }
        event.preventDefault();
    }

    render() {
        const {classes} = this.props;
        const showInfoText = !this.state.signUpSuccessful;
        const showSuccessText = this.state.signUpSuccessful;
        const showErrorText = this.state.submitError;
        return (<Container className={classes.signUpPanel}>
            <Typography variant="h3"
            >
                Sign up
            </Typography>
            <form className={classes.signUpForm} onSubmit={this.tryToRegister}
            >
                <Grid container
                      spacing={2}
                >
                    <Grid item
                          xs={12}
                    >
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="mailAddress"
                            label="Please enter your mail address ..."
                            type="email"
                            value={this.state.emailField.value}
                            helperText={this.state.emailField.helperText}
                            error={this.state.emailField.error}
                            autoFocus
                            disabled={this.state.signUpSuccessful}
                            onChange={this.handleEmailAddressChange}
                        />
                    </Grid>
                    <Grid item
                          xs={12}
                    >
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="confirmedMailAddress"
                            label="... and confirm it once more"
                            type="email"
                            value={this.state.confirmEmailField.value}
                            helperText={this.state.confirmEmailField.helperText}
                            error={this.state.confirmEmailField.error}
                            disabled={this.state.signUpSuccessful}
                            onChange={this.handleConfirmedEmailAddressChange}
                        />
                    </Grid>
                </Grid>
                {showInfoText && (
                    <Typography className={classes.infoText}
                    >
                        Please enter your mail address and you will receive an
                        activation mail.
                    </Typography>
                )}
                {showSuccessText && (
                    <Typography className={classes.infoText}>
                        Thanks for signing up. Look at your inbox for an incoming activation mail.
                    </Typography>
                )}
                {showErrorText && (
                    <Typography className={classes.infoText} color="error">
                        {this.state.submitError}
                    </Typography>
                )}
                <Button type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submitButton}
                        disabled={this.state.signUpSuccessful}
                >Sign up</Button>
            </form>
        </Container>);
    }
}

export default withStyles(styles)(SignUpPanel);