import Button from '@material-ui/core/Button';
import React from 'react';
import { Link } from 'react-router-dom';

const SignInLink = React.forwardRef((props, ref) => (
    <Link innerRef={ ref }
          to="/login" { ...props }/>
));

export default function SignInButton(props) {
    return (
        <Button variant="outlined"
                color="primary"
                href="#"
                component={ SignInLink }
                { ...props }
        >
            Sign In
        </Button>
    );
}