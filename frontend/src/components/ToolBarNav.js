import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import SignUpButton from './SignUpButton';
import SignInButton from './SignInButton';

const styles = makeStyles(theme => ({
    navButton: {
        margin: theme.spacing(1, 1.5)
    }
}));

export function ToolBarNav() {
    const classes = styles();
    return (
        <nav>
            <SignUpButton className={ classes.navButton }/>
            <SignInButton className={ classes.navButton }/>
        </nav>
    );
}