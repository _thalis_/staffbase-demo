import React from 'react';
import {Page} from './Page';
import {Typography} from '@material-ui/core';
import Container from '@material-ui/core/Container';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import SignInButton from '../components/SignInButton';
import SignUpButton from '../components/SignUpButton';

const styles = makeStyles(theme => ({
    welcomeContent: {
        padding: theme.spacing(8, 0, 6)
    },
    buttonPanel: {
        padding: theme.spacing(8, 0, 6)
    }
}));

export default function WelcomePage() {
    const classes = styles();
    return (
        <Page>
            <Container maxWidth="sm"
                       component="main"
                       className={ classes.welcomeContent }
            >
                <Typography component="h1"
                            variant="h2"
                            align="center"
                            color="textPrimary"
                            gutterBottom
                >
                    Welcome!
                </Typography>
                <Typography component="p"
                            variant="h5"
                            align="center"
                            color="textSecondary"
                >
                    You search for answers or want to help someone? Here you are right.
                </Typography>
                <Grid container
                      spacing={ 2 }
                      className={ classes.buttonPanel }
                >
                    <Grid item
                          xs={ 12 }
                          sm={ 6 }
                    >
                        <SignUpButton fullWidth/>
                    </Grid>
                    <Grid item
                          xs={ 12 }
                          sm={ 6 }
                    >
                        <SignInButton fullWidth/>
                    </Grid>
                </Grid>
            </Container>
        </Page>
    );
}