import React from 'react';
import TodoAppBar from '../components/QuandaAppBar';
import {CssBaseline} from '@material-ui/core';

export function Page(props) {
    return (
        <React.Fragment>
            <CssBaseline/>
            <TodoAppBar/>
            { props.children }
        </React.Fragment>
    );
}