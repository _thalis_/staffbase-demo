import React from 'react';
import { Page } from './Page';

export default function ActivationPage() {
    return (
        <Page>
            Activate your user
        </Page>
    );
}