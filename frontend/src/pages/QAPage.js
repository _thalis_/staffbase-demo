import React from 'react';
import {Page} from './Page';

export default function QAPage() {
    return (
        <Page>
            Welcome at QuAndA!
        </Page>
    );
}