import React from 'react';
import { Page } from './Page';
import SignUpPanel from '../components/SignUpPanel';

export default function SignUpPage() {
    return (
        <Page>
            <SignUpPanel/>
        </Page>
    );
}