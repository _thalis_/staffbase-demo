import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import SignInPage from './pages/SignInPage';
import WelcomePage from './pages/WelcomePage';
import ActivationPage from './pages/ActivationPage';
import SignUpPage from './pages/SignUpPage';
import QAPage from "./pages/QAPage";

const router = (
    <Router>
        <div className="route">
            <Switch>
                <Route path="/login"
                       component={ SignInPage }
                />
                <Route path="/todos" component={QAPage}/>
                <Route path="/activation/:token" component={ActivationPage}/>
                <Route path="/register"
                       component={ SignUpPage }
                />
                <Route component={ WelcomePage }/>
            </Switch>
        </div>
    </Router>
);

ReactDOM.render(router, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
