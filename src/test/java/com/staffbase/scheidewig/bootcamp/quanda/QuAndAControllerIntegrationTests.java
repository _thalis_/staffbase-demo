package com.staffbase.scheidewig.bootcamp.quanda;

import com.staffbase.scheidewig.bootcamp.quanda.adapter.QuAndAController;
import com.staffbase.scheidewig.bootcamp.quanda.application.*;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Answer;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;
import com.staffbase.scheidewig.bootcamp.quanda.util.EndpointUriUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                QuandaApplication.class
        })
@ActiveProfiles("test")
public class QuAndAControllerIntegrationTests {

    private static final String ASKING_USER = "asking_user";
    private static final String COMMENTING_USER1 = "commenting_user_1";
    private static final String ANSWERING_USER1 = "answering_user_1";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCreateQuestionsAnswersCommentsAndListThem() {

        // given
        final Question question = createNewQuestion(
                newQuestion(ASKING_USER,
                        "A simple question",
                        "What is better? Green or blue?")).getBody();
        final String questionId = question.getQuestionId();
        createQuestionComment(
                questionId,
                newComment(COMMENTING_USER1, "Do you mean colors?")
        );
        createQuestionComment(
                questionId,
                newComment(ASKING_USER, "Yeah, exactly :)")
        );

        final Answer answer = createNewAnswer(
                questionId,
                newAnswer(ANSWERING_USER1, "Blue, of course. Blue planet, you know?"))
                .getBody();
        final String answerId = answer.getAnswerId();

        createAnswerComment(
                answerId,
                newComment(COMMENTING_USER1, "Maybe you're right.")
        );
        createAnswerComment(
                answerId,
                newComment(ASKING_USER, "I thought about it again and I think you're right. Thanks!")
        );

        // when
        final ResponseEntity<QuestionWithAnswersAndComments> response =
                getAllAboutQuestion(question.getQuestionId());

        // then
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody(), is(notNullValue()));
        final QuestionWithAnswersAndComments completeQuestion =
                response.getBody();
        assertThat(completeQuestion.getQuestion().getTitle(), is("A simple question"));
        assertThat(completeQuestion.getQuestion().getQuestionText(),
                is("What is better? Green or blue?"));
        assertThat(completeQuestion.getQuestion().getQuestionId(), is(questionId));
        assertThat(completeQuestion.getQuestion().getAskingUser(), is(ASKING_USER));

        assertThat(completeQuestion.getComments().size(), is(2));
        assertThat(completeQuestion.getComments().get(0).getComment(), is("Do you mean colors?"));
        assertThat(completeQuestion.getComments().get(0).getCommentingUser(),
                is(COMMENTING_USER1));

        assertThat(completeQuestion.getAnswersWithComments().size(), is(1));
        final AnswerWithComments answerWithComments = completeQuestion.getAnswersWithComments().get(0);
        assertThat(answerWithComments.getAnswer().getAnswerText(), containsString("Blue planet"));
        assertThat(answerWithComments.getComments().size(), is(2));
        assertThat(answerWithComments.getComments().get(1).getCommentingUser(), is(ASKING_USER));
        assertThat(answerWithComments.getComments().get(1).getComment(), containsString("Thanks!"));
    }

    private NewQuestion newQuestion(String userId, String title, String questionText) {
        return new NewQuestion(userId, title, questionText);
    }

    private NewAnswer newAnswer(String userId, String answerText) {
        return new NewAnswer(userId, answerText);
    }

    private NewComment newComment(String userId, String commentText) {
        return new NewComment(userId, commentText);
    }

    private ResponseEntity<Question> createNewQuestion(NewQuestion newQuestion) {
        final URI newQuestionUri =
                EndpointUriUtil.endpointUriOfControllerMethod(QuAndAController.class,
                        quAndAController -> quAndAController.askQuestion(newQuestion));
        return restTemplate.postForEntity(newQuestionUri, newQuestion, Question.class);
    }

    private ResponseEntity<Comment> createQuestionComment(String questionId, NewComment newComment) {
        final URI newCommentUri =
                EndpointUriUtil.endpointUriOfControllerMethod(QuAndAController.class,
                        quAndAController -> quAndAController.commentQuestion(questionId, newComment));
        return restTemplate.postForEntity(newCommentUri, newComment, Comment.class);
    }

    private ResponseEntity<Answer> createNewAnswer(String questionId, NewAnswer newAnswer) {
        final URI newAnswerUri =
                EndpointUriUtil.endpointUriOfControllerMethod(QuAndAController.class,
                        quAndAController -> quAndAController.answerQuestion(questionId, newAnswer));
        return restTemplate.postForEntity(newAnswerUri, newAnswer, Answer.class);
    }

    private ResponseEntity<Comment> createAnswerComment(String answerId, NewComment newComment) {
        final URI newCommentUri =
                EndpointUriUtil.endpointUriOfControllerMethod(QuAndAController.class,
                        quAndAController -> quAndAController.commentAnswer(answerId, newComment));
        return restTemplate.postForEntity(newCommentUri, newComment, Comment.class);
    }

    private ResponseEntity<QuestionWithAnswersAndComments> getAllAboutQuestion(String questionId) {
        final URI allAboutAQuestionUri =
                EndpointUriUtil.endpointUriOfControllerMethod(QuAndAController.class,
                        quAndAController -> quAndAController.getQuestion(questionId));
        return restTemplate.getForEntity(allAboutAQuestionUri, QuestionWithAnswersAndComments.class);
    }

}
