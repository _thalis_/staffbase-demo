package com.staffbase.scheidewig.bootcamp.quanda.util;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.function.Consumer;

public class EndpointUriUtil {

    public static <T> URI endpointUriOfControllerMethod(
            Class<T> controllerClass, Consumer<T> methodCall) {
        final T controllerUsage = MvcUriComponentsBuilder.on(controllerClass);
        methodCall.accept(controllerUsage);
        return MvcUriComponentsBuilder.fromMethodCall(UriComponentsBuilder.fromPath("/"),
                controllerUsage).build().encode().toUri();
    }

}
