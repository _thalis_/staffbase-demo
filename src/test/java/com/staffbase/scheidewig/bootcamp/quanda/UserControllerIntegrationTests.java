package com.staffbase.scheidewig.bootcamp.quanda;

import com.staffbase.scheidewig.bootcamp.quanda.adapter.UserController;
import com.staffbase.scheidewig.bootcamp.quanda.application.ApplicationProperties;
import com.staffbase.scheidewig.bootcamp.quanda.events.UserRegistered;
import com.staffbase.scheidewig.bootcamp.quanda.util.EndpointUriUtil;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.mockito.Mockito.verify;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {
                QuandaApplication.class
        })
@ActiveProfiles("test")
class UserControllerIntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ApplicationProperties applicationProperties;
    @MockBean
    private ApplicationEventPublisher eventPublisher;

    @Test
    void testSelfRegistrationProcess() {
        // given
        final String testMailAddress = "john.doe@example.com";
        final Instant testStartTime = Instant.now();

        final ArgumentCaptor<UserRegistered> userRegisteredEventCaptor =
                ArgumentCaptor.forClass(UserRegistered.class);
        // register ...
        final ResponseEntity<String> registrationResponse = register(testMailAddress);
        assertThat(registrationResponse.getStatusCode(), is(equalTo(HttpStatus.OK)));
        verify(eventPublisher).publishEvent(userRegisteredEventCaptor.capture());

        final UserRegistered userRegisteredEvent = userRegisteredEventCaptor.getValue();
        assertThat(userRegisteredEvent.getEmailAddress(), is(equalTo(testMailAddress)));
        assertThat(userRegisteredEvent.getActivationLink(), is(not(emptyString())));
        assertThat(userRegisteredEvent.getRegistrationTime(), allOf(is(notNullValue()),
                is(greaterThan(testStartTime))));
        final String activationLink = Optional.ofNullable(applicationProperties.getActivationLink()
                .match(userRegisteredEvent.getActivationLink())
                .get("token")).orElseThrow(() -> new AssertionError("No valid activation link"));

        // activate ...
        final ResponseEntity<String> activationResponse = activate(activationLink, "test123!", "test123!");
        assertThat(activationResponse.getStatusCode(), is(equalTo(HttpStatus.NO_CONTENT)));


    }

    private ResponseEntity<String> register(String mailAddress) {
        final URI registrationUri = EndpointUriUtil.endpointUriOfControllerMethod(UserController.class,
                userController -> userController.register(null, null));
        final Map<String, String> params = new HashMap<>();
        params.put("mailAddress", mailAddress);
        params.put("repeatedMailAddress", mailAddress);
        return restTemplate.postForEntity(registrationUri, params, String.class);
    }

    private ResponseEntity<String> activate(String activationToken, String password, String confirmedPassword) {
        final URI activationUri =
                EndpointUriUtil.endpointUriOfControllerMethod(UserController.class,
                        userController -> userController.activate(null, null, null));
        final Map<String, String> params = new HashMap<>();
        params.put("activationToken", activationToken);
        params.put("password", password);
        params.put("confirmedPassword", confirmedPassword);
        return restTemplate.postForEntity(activationUri, params, String.class);
    }

}
