package com.staffbase.scheidewig.bootcamp.quanda.adapter;

import com.staffbase.scheidewig.bootcamp.quanda.application.UserService;
import com.staffbase.scheidewig.bootcamp.quanda.domain.User;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void login(
            @RequestPart String mailAddress,
            @RequestPart String password) {

    }

    @PostMapping("/logout")
    public void logout() {

    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<List<String>> register(
            @RequestPart String mailAddress,
            @RequestPart String confirmedMailAddress) {

        final Validation<Seq<String>, User> registrationResult =
                this.userService.registerNewUser(mailAddress, confirmedMailAddress);

        if (registrationResult.isInvalid()) {
            return ResponseEntity.badRequest().body(registrationResult.getError().toList());
        }

        return ResponseEntity.ok().body(List.of("User successfully registered"));
    }

    @PostMapping("/activate")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<String>> activate(
            @RequestPart String activationToken,
            @RequestPart String password,
            @RequestPart String confirmedPassword
    ) {
        final Validation<Seq<String>, Option<User>> activationResult =
                userService.activateNewUser(activationToken, password, confirmedPassword);

        if (activationResult.isInvalid()) {
            return ResponseEntity.badRequest().body(activationResult.getError().toList());
        }

        return loginResponse(ResponseEntity.ok().body(List.of("User successfully activated")));
    }

    private <T> ResponseEntity<T> loginResponse(ResponseEntity<T> responseEntity) {
        return responseEntity;
    }

}
