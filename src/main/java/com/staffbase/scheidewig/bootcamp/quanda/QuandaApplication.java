package com.staffbase.scheidewig.bootcamp.quanda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class QuandaApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuandaApplication.class, args);
	}

}
