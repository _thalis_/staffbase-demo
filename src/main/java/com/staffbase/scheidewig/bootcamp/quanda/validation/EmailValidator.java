package com.staffbase.scheidewig.bootcamp.quanda.validation;

import io.vavr.Function1;
import io.vavr.control.Validation;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;

public final class EmailValidator {

    private static final org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator hibernateEmailValidator
            = new org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator();

    private EmailValidator() {
    }

    public static Validation<String, CharSequence> validateMailAddress(
            CharSequence mailAddress) {
        return StringValidator.validateNotBlank(mailAddress, (blankMailAddress) -> "The mail address is empty")
                .flatMap(nonBlankMailAddress ->
                        validateAddress(nonBlankMailAddress, (invalidMailAddress) -> "The mail address is invalid"));
    }

    public static <E> Validation<E, CharSequence> validateMailAddress(
            CharSequence mailAddress,
            Function1<CharSequence, E> constraintMessage) {
        return StringValidator.validateNotBlank(mailAddress, constraintMessage)
                .flatMap(nonBlankMailAddress -> validateAddress(nonBlankMailAddress, constraintMessage));
    }

    private static <E> Validation<E, CharSequence> validateAddress(
            CharSequence mailAddress,
            Function1<CharSequence, E> constraintMessage) {
        final boolean validMailAddress =
                hibernateEmailValidator.isValid(mailAddress, Mockito.mock(ConstraintValidatorContext.class));
        if (validMailAddress) {
            return Validation.valid(mailAddress);
        }
        return Validation.invalid(constraintMessage.apply(mailAddress));
    }
}
