package com.staffbase.scheidewig.bootcamp.quanda.domain;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "USER", schema = "\"USER\"", indexes = {
        @Index(name = "IDX_MAIL", columnList = "MAIL")
})
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "USER_ID", unique = true, nullable = false)
    private String userId;

    @Column(name = "MAIL", unique = true, nullable = false)
    private String emailAddress;

    @Column(name = "PASSWORD")
    private String hashedPassword;

    @Column(name = "ACTIVATION_TOKEN")
    private String activationToken;

    @Column(name = "REGISTRATION_TIME", nullable = false)
    private Instant registrationTime;

    @Column(name = "ACTIVATION_TIME")
    private Instant activationTime;

    /**
     * Constructor for JPA.
     */
    protected User() {
        super();
    }

    public User(String emailAddress) {
        this.userId = UUID.randomUUID().toString();
        this.emailAddress = emailAddress;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public Instant getRegistrationTime() {
        return registrationTime;
    }

    public Instant getActivationTime() {
        return activationTime;
    }

    public boolean isAlreadyActivated() {
        return StringUtils.isNotBlank(this.activationToken);
    }

    public void activate(String hashedPassword) {
        if (isAlreadyActivated()) {
            return;
        }
        this.activationTime = Instant.now();
        this.activationToken = null;
        this.hashedPassword = hashedPassword;
    }

    public void startRegistration(String activationToken) {
        this.activationToken = activationToken;
        this.registrationTime = Instant.now();
    }
}
