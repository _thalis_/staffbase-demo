package com.staffbase.scheidewig.bootcamp.quanda.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.util.UriTemplate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.Instant;

@ConfigurationProperties("staffbase.quanda")
@Getter
@Setter
@ToString
@Validated
public class ApplicationProperties {

    /**
     * Defines the length of the random alphanumeric activation token.
     */
    @Min(value = 5, message = "The activation token must be at least 5 characters long.")
    private int activationTokenSize;

    /**
     * Defines the sender mail address for system sent mails.
     */
    @NotEmpty
    private String mailFromAddress;

    /**
     * The time inactive registered users will be deleted again.
     */
    @NotNull
    private Duration inActiveRegisteredUserEvictionTime;

    /**
     * The link to the activation page.
     */
    @NotNull
    private UriTemplate activationLink;

    /**
     * Returns the time when the activation will expire.
     */
    public Instant activationExpirationTime(Instant timeOfRegistration) {
        return timeOfRegistration.plus(inActiveRegisteredUserEvictionTime);
    }

}
