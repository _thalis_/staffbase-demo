package com.staffbase.scheidewig.bootcamp.quanda.events;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

import java.time.Instant;

@ToString
@Getter
public class UserActivated extends ApplicationEvent {

    private final String userId;
    private final String emailAddress;
    private final Instant activationTime;

    public UserActivated(Object source, String userId, String emailAddress, Instant activationTime) {
        super(source);
        this.userId = userId;
        this.emailAddress = emailAddress;
        this.activationTime = activationTime;
    }
}
