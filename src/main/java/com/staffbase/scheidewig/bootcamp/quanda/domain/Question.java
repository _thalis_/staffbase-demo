package com.staffbase.scheidewig.bootcamp.quanda.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "QUESTION", schema = "QUANDA")
public class Question {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(name = "QUESTION_ID", nullable = false, unique = true)
    private String questionId;

    @Column(name = "TITLE", nullable = false, length = 500)
    private String title;

    @Column(name = "QUESTION_TEXT", nullable = false, length = 7000)
    private String questionText;

    @Column(name = "ASKING_USER", nullable = false)
    private String askingUser;

    @Column(name = "CREATION_TIME", nullable = false)
    private Instant creationTime;

    public Question(String askingUser, String title, String questionText) {
        this.questionId = UUID.randomUUID().toString();
        this.askingUser = askingUser;
        this.title = title;
        this.questionText = questionText;
        this.creationTime = Instant.now();
    }

    // JPA constructor.
    protected Question() {
        super();
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getTitle() {
        return title;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getAskingUser() {
        return askingUser;
    }

    public Instant getCreationTime() {
        return creationTime;
    }
}
