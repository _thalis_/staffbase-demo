package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.User;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.springframework.data.repository.CrudRepository;

import java.time.Instant;

public interface UserRepository extends CrudRepository<User, Long> {

    Option<User> findByEmailAddress(String emailAddress);

    Seq<User> findAllByActivationTokenIsNotNullAndRegistrationTimeBefore(Instant timeOfEvictedRegistrations);

    Option<User> findByActivationToken(String activationToken);

    Option<User> findByEmailAddressAndHashedPassword(String emailAddress, String hashedPassword);

}
