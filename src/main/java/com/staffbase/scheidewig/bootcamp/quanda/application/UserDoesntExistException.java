package com.staffbase.scheidewig.bootcamp.quanda.application;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "User and/or password incorrect")
public class UserDoesntExistException extends RuntimeException {

    public UserDoesntExistException() {
        super();
    }
}
