package com.staffbase.scheidewig.bootcamp.quanda.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "ANSWER", schema = "QUANDA")
public class Answer {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(name = "ANSWER_ID", nullable = false)
    private String answerId;

    @Column(name = "QUESTION_ID", nullable = false)
    private String questionId;

    @Column(name = "ANSWER_TEXT", nullable = false, length = 7000)
    private String answerText;

    @Column(name = "ANSWERING_USER", nullable = false)
    private String answeringUser;

    @Column(name = "CREATION_TIME", nullable = false)
    private Instant creationTime;

    public Answer(String postUser, String questionId, String answerText) {
        this.answerId = UUID.randomUUID().toString();
        this.answeringUser = postUser;
        this.questionId = questionId;
        this.answerText = answerText;
        this.creationTime = Instant.now();
    }

    // JPA constructor.
    protected Answer() {
        super();
    }

    public String getAnswerId() {
        return answerId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getAnswerText() {
        return answerText;
    }

    public String getAnsweringUser() {
        return answeringUser;
    }

    public Instant getCreationTime() {
        return creationTime;
    }
}
