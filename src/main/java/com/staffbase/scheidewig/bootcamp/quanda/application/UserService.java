package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.User;
import com.staffbase.scheidewig.bootcamp.quanda.events.UserActivated;
import com.staffbase.scheidewig.bootcamp.quanda.events.UserRegistered;
import com.staffbase.scheidewig.bootcamp.quanda.validation.EmailValidator;
import com.staffbase.scheidewig.bootcamp.quanda.validation.StringValidator;
import io.vavr.Function1;
import io.vavr.Function2;
import io.vavr.Tuple;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.regex.Pattern;

import static java.lang.String.format;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final ApplicationProperties applicationProperties;
    private final ApplicationEventPublisher eventPublisher;
    private final PasswordEncoder passwordEncoder;
    private final Function1<String, Validation<Seq<String>, String>> tokenValidator;
    private final Function2<String, String, Validation<Seq<String>, String>> passwordValidator;

    @Autowired
    public UserService(
            UserRepository userRepository,
            ApplicationProperties applicationProperties,
            ApplicationEventPublisher eventPublisher,
            PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.applicationProperties = applicationProperties;
        this.eventPublisher = eventPublisher;
        this.passwordEncoder = passwordEncoder;
        this.tokenValidator = (token) ->
                Validation.combine(
                        StringValidator.validateNotBlank(token, () -> "The authentication token is blank"),
                        StringValidator.validatePattern(
                                Pattern.compile(format("[\\p{Alnum}]{%d}",
                                        applicationProperties.getActivationTokenSize())),
                                token)
                ).ap((nonBlankToken, validToken) -> validToken);
        this.passwordValidator =
                (password, confirmedPassword) -> Validation.combine(
                        StringValidator.validateNotBlank(password, () -> "The password must not be empty"),
                        StringValidator.validateNotBlank(confirmedPassword, () -> "The confirmed password must "
                                + "not be empty"),
                        StringValidator.validateEqual(Tuple.of(password, confirmedPassword),
                                () -> "The password and the confirmed password must be equal"),
                        StringValidator.validatePattern(
                                Pattern.compile("\\S+\\s\\S+"),
                                password,
                                (pass, pattern) -> "The password must not contain any whitespaces"),
                        StringValidator.validatePattern(Pattern.compile("\\S{10,}"),
                                password,
                                (pass, pattern) -> "The password must be at least 10 characters long"),
                        StringValidator.validatePattern(
                                Pattern.compile("\\d{3,}"),
                                password,
                                (pass, pattern) -> "The password must contain at least 3 digits")
                ).ap((pass1, pass2, pass3, pass4, pass5, validPassword) -> validPassword);
    }

    public Validation<Seq<String>, User> registerNewUser(String emailAddress, String confirmedEmailAddress) {

        return Validation.combine(
                StringValidator.validateNotBlank(emailAddress, () -> "The email address must not be blank"),
                StringValidator.validateNotBlank(confirmedEmailAddress,
                        () -> "The confirmed email address must not be blank"),
                StringValidator.validateEqual(Tuple.of(emailAddress, confirmedEmailAddress),
                        () -> "The email address and the confirmed email address must be equal"),
                EmailValidator.validateMailAddress(emailAddress)
        ).ap((ma, cma, mailTuple, validEmailAddress) -> {
            final Option<User> registeredUser = userRepository.findByEmailAddress(emailAddress);
            if (registeredUser.isDefined() && registeredUser.get().isAlreadyActivated()) {
                return registeredUser.get();
            }

            final User userInRegistration = registeredUser.getOrElse(() -> new User(emailAddress));
            final String activationToken = createRandomActivationToken();
            userInRegistration.startRegistration(activationToken);

            userRepository.save(userInRegistration);
            eventPublisher.publishEvent(new UserRegistered(
                    this,
                    userInRegistration.getEmailAddress(),
                    createActivationLink(activationToken),
                    userInRegistration.getRegistrationTime()));

            return userInRegistration;
        });
    }

    public Validation<Seq<String>, Option<User>> activateNewUser(
            String activationToken,
            String plainPassword,
            String confirmedPassword) {

        return Validation.combine(
                tokenValidator.apply(activationToken),
                passwordValidator.apply(plainPassword, confirmedPassword)
        ).ap((token, password) ->
                userRepository.findByActivationToken(token)
                        .map(registeredUser -> {
                            registeredUser.activate(passwordEncoder.encode(password));
                            return registeredUser;
                        })
                        .peek(activatedUser ->
                                eventPublisher.publishEvent(
                                        new UserActivated(
                                                this,
                                                activatedUser.getUserId(),
                                                activatedUser.getEmailAddress(),
                                                activatedUser.getActivationTime()
                                        )
                                )
                        )
        ).bimap(
                errors -> errors.flatMap(Function1.identity()),
                Function1.identity()
        );
    }

    @Scheduled(initialDelay = 5000, fixedRate = 180000)
    public void removeInactiveRegistrations() {

        LOGGER.debug("Removing inactive registrations ...");
        final Instant timeBeforeRegistrationsAreInactive =
                applicationProperties.activationExpirationTime(Instant.now());
        final Seq<User> inactiveRegisteredUsers =
                userRepository.findAllByActivationTokenIsNotNullAndRegistrationTimeBefore(
                        timeBeforeRegistrationsAreInactive);
        if (inactiveRegisteredUsers.size() > 0) {
            userRepository.deleteAll(inactiveRegisteredUsers);
            LOGGER.info(format("%s inactive registered user(s) removed", inactiveRegisteredUsers.size()));
        }
    }

    private String createRandomActivationToken() {
        return RandomStringUtils.randomAlphanumeric(applicationProperties.getActivationTokenSize());
    }

    private String createActivationLink(String activationToken) {
        return applicationProperties.getActivationLink().expand(activationToken).toString();
    }
}
