package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;
import io.vavr.collection.Seq;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {

    Seq<Comment> findAllByPostIdOrderByCreationTimeAsc(String postId);

}
