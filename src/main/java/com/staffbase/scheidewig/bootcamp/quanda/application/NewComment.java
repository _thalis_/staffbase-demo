package com.staffbase.scheidewig.bootcamp.quanda.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
public class NewComment {

    @NotBlank
    private String userId;
    @NotBlank
    private String commentText;

    public NewComment(@NotBlank String userId, @NotBlank String commentText) {
        this.userId = userId;
        this.commentText = commentText;
    }

    @SuppressWarnings("unused") // Used by Jackson
    public NewComment() {
        super();
    }
}
