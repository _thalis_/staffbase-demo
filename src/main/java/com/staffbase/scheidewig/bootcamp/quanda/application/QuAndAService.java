package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Answer;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;
import io.vavr.collection.Seq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class QuAndAService {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public QuAndAService(
            QuestionRepository questionRepository,
            AnswerRepository answerRepository,
            CommentRepository commentRepository) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.commentRepository = commentRepository;
    }

    public Question askNewQuestion(String askingUser, String title, String question) {
        final Question newQuestion = new Question(askingUser, title, question);
        this.questionRepository.save(newQuestion);
        return newQuestion;
    }

    public Answer answerQuestion(String answeringUser, String questionId, String answerText) {
        final Answer newAnswer = new Answer(answeringUser, questionId, answerText);
        this.answerRepository.save(newAnswer);
        return newAnswer;
    }

    public Comment commentPost(String commentingUser, String postId, String commentText) {
        final Comment newComment = new Comment(commentingUser, postId, commentText);
        this.commentRepository.save(newComment);
        return newComment;
    }

    public Seq<Comment> getCommentsOfPost(String postId) {
        return this.commentRepository.findAllByPostIdOrderByCreationTimeAsc(postId);
    }

    public Page<Question> getQuestions(Pageable pageable) {
        return this.questionRepository.findAll(pageable);
    }

    public Seq<Answer> getAllAnswersForQuestion(String questionId) {
        return this.answerRepository.findAllByQuestionIdOrderByCreationTimeAsc(questionId);
    }

    public Question getQuestion(String questionId) {
        return this.questionRepository.findByQuestionId(questionId)
                .getOrElseThrow(() -> new ResourceNotFoundException("Question does not exist"));
    }
}
