package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;
import io.vavr.control.Option;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {

    Page<Question> findAll(Pageable pageable);

    Option<Question> findByQuestionId(String questionId);

}
