package com.staffbase.scheidewig.bootcamp.quanda.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
public class NewAnswer {

    @NotBlank
    private String userId;
    @NotBlank
    private String answerText;

    public NewAnswer(@NotBlank String userId, @NotBlank String answerText) {
        this.userId = userId;
        this.answerText = answerText;
    }

    @SuppressWarnings("unused") // Used by Jackson
    public NewAnswer() {
        super();
    }
}
