package com.staffbase.scheidewig.bootcamp.quanda.config;

import io.vavr.Tuple;
import io.vavr.collection.Stream;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleValidationExceptions(
            ConstraintViolationException notValidEx
    ) {
        return ResponseEntity.badRequest().body(Stream.ofAll(notValidEx.getConstraintViolations())
                .map(violEx -> Tuple.of(violEx.getPropertyPath(), violEx.getMessage()))
                .mkString(","));
    }
}
