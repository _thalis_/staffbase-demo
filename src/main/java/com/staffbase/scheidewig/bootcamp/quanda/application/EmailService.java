package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.events.UserActivated;
import com.staffbase.scheidewig.bootcamp.quanda.events.UserRegistered;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import static java.lang.String.format;

@Service
public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender javaMailSender;
    private final ApplicationProperties applicationProperties;

    public EmailService(
            JavaMailSender javaMailSender,
            ApplicationProperties applicationProperties) {
        this.javaMailSender = javaMailSender;
        this.applicationProperties = applicationProperties;
    }

    /**
     * When a user registered he will receive a registration mail where she can activate herself.
     */
    @EventListener
    @Async
    public void sendUserRegistrationMail(UserRegistered userRegisteredEvent) {
        final LocalDateTime activationExpiration =
                LocalDateTime.ofInstant(
                        applicationProperties.activationExpirationTime(
                                userRegisteredEvent.getRegistrationTime()),
                        ZoneId.systemDefault());
        MimeMessagePreparator preparator = mimeMessage -> {
            mimeMessage.setSubject("Your registration at Q&A app");
            mimeMessage.setFrom(applicationProperties.getMailFromAddress());
            mimeMessage.setRecipients(Message.RecipientType.TO, userRegisteredEvent.getEmailAddress());
            mimeMessage.setText(format("Welcome to the Q&A app!\n" +
                            "Please follow this link %s to activate your account.\n" +
                            "The registration will expire at %s",
                    userRegisteredEvent.getActivationLink(),
                    activationExpiration.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))
            ));
        };
        try {
            javaMailSender.send(preparator);
        } catch (final MailException mailEx) {
            LOGGER.warn(format("The registration mail could not be sent for %s",
                    userRegisteredEvent.getEmailAddress()), mailEx);
        }
    }

    @EventListener
    @Async
    public void sendUserActivationMail(UserActivated userActivatedEvent) {
        MimeMessagePreparator preparator = mimeMessage -> {
            mimeMessage.setSubject("Welcome at QuAndA!");
            mimeMessage.setFrom(applicationProperties.getMailFromAddress());
            mimeMessage.setRecipients(Message.RecipientType.TO, userActivatedEvent.getEmailAddress());
            mimeMessage.setText("Welcome to QuAndA, your place to raise questions or help others!");
        };
        try {
            javaMailSender.send(preparator);
        } catch (final MailException mailEx) {
            LOGGER.warn(format("The activation mail could not be sent for %s",
                    userActivatedEvent.getEmailAddress()), mailEx);
        }
    }

}
