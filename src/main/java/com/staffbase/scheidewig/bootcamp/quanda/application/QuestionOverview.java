package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;
import org.apache.commons.lang3.StringUtils;

public class QuestionOverview {

    private static final int TITLE_MAX_LENGTH = 80;
    private static final int DESCRIPTION_MAX_LENGTH = 100;

    private final String questionId;
    private final String title;
    private final String shortDescription;

    public QuestionOverview(Question question) {
        this.questionId = question.getQuestionId();
        this.title = shortenText(question.getTitle(), TITLE_MAX_LENGTH);
        this.shortDescription = shortenText(question.getQuestionText(), DESCRIPTION_MAX_LENGTH);
    }

    public String getQuestionId() {
        return questionId;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    private String shortenText(String text, int maxLength) {
        if (text.length() > maxLength) {
            return String.format("%s ...", StringUtils.substring(text, 0, maxLength));
        } else {
            return text;
        }
    }
}
