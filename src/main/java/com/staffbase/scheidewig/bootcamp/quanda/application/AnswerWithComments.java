package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Answer;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;

import java.util.List;

public class AnswerWithComments {

    private final Answer answer;
    private final List<Comment> comments;

    public AnswerWithComments(
            Answer answer,
            List<Comment> comments) {
        this.answer = answer;
        this.comments = comments;
    }

    public Answer getAnswer() {
        return answer;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
