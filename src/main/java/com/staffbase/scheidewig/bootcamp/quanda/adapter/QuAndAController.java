package com.staffbase.scheidewig.bootcamp.quanda.adapter;

import com.staffbase.scheidewig.bootcamp.quanda.application.*;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Answer;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/api/quanda")
public class QuAndAController {

    private final QuAndAService quAndAService;

    public QuAndAController(QuAndAService quAndAService) {
        this.quAndAService = quAndAService;
    }

    @PostMapping("/question")
    public Question askQuestion(@RequestBody @Valid NewQuestion newQuestion) {
        return this.quAndAService.askNewQuestion(
                newQuestion.getUserId(), newQuestion.getTitle(), newQuestion.getQuestionText());
    }

    @PostMapping("/question/{questionId}/answer")
    public Answer answerQuestion(
            @PathVariable @NotEmpty String questionId,
            @RequestBody @Valid NewAnswer newAnswer) {
        return this.quAndAService.answerQuestion(
                newAnswer.getUserId(), questionId, newAnswer.getAnswerText());
    }

    @PostMapping("/question/{questionId}/comment")
    public Comment commentQuestion(
            @PathVariable @NotEmpty String questionId,
            @RequestBody @Valid NewComment newComment
    ) {
        return this.quAndAService.commentPost(
                newComment.getUserId(), questionId, newComment.getCommentText());
    }

    @PostMapping("/answer/{answerId}/comment")
    public Comment commentAnswer(
            @PathVariable @NotEmpty String answerId,
            @RequestBody @Valid NewComment newComment
    ) {
        return this.quAndAService.commentPost(
                newComment.getUserId(), answerId, newComment.getCommentText());
    }

    @GetMapping("/question/{questionId}")
    public QuestionWithAnswersAndComments getQuestion(@PathVariable @NotEmpty String questionId) {
        return Option.of(questionId)
                .map(this.quAndAService::getQuestion)
                .map(question -> {
                    final Seq<Comment> commentsForQuestion =
                            this.quAndAService.getCommentsOfPost(question.getQuestionId());
                    final Seq<AnswerWithComments> answersWithComments =
                            this.quAndAService.getAllAnswersForQuestion(questionId)
                                    .map(answer -> {
                                        final Seq<Comment> commentsForAnswer =
                                                this.quAndAService.getCommentsOfPost(answer.getAnswerId());
                                        return new AnswerWithComments(answer, commentsForAnswer.toJavaList());
                                    });
                    return new QuestionWithAnswersAndComments(
                            question, commentsForQuestion.toJavaList(), answersWithComments.toJavaList());
                }).getOrElseThrow(() -> new ResourceNotFoundException("Question not found"));
    }


    @GetMapping("/questions")
    public Page<QuestionOverview> getQuestions(Pageable pageable) {
        return this.quAndAService.getQuestions(pageable).map(QuestionOverview::new);
    }

}
