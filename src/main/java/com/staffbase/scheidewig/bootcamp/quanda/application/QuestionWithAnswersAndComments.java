package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Comment;
import com.staffbase.scheidewig.bootcamp.quanda.domain.Question;

import java.util.List;

public class QuestionWithAnswersAndComments {

    private final Question question;
    private final List<Comment> comments;
    private final List<AnswerWithComments> answersWithComments;

    public QuestionWithAnswersAndComments(
            Question question,
            List<Comment> comments,
            List<AnswerWithComments> answersWithComments
    ) {
        this.question = question;
        this.comments = comments;
        this.answersWithComments = answersWithComments;
    }

    public Question getQuestion() {
        return question;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<AnswerWithComments> getAnswersWithComments() {
        return answersWithComments;
    }
}
