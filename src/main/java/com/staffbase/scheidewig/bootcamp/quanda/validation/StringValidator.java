package com.staffbase.scheidewig.bootcamp.quanda.validation;

import io.vavr.Function0;
import io.vavr.Function2;
import io.vavr.Tuple2;
import io.vavr.control.Validation;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

import static java.lang.String.format;

public final class StringValidator {

    private StringValidator() {
    }

    public static <E> Validation<E, Tuple2<String, String>> validateEqual(
            Tuple2<String, String> pair,
            Function0<E> constraintMessage) {
        return StringUtils.equals(pair._1, pair._2) ?
                Validation.valid(pair) : Validation.invalid(constraintMessage.apply());
    }

    public static <E> Validation<E, String> validateNotBlank(
            String string,
            Function0<E> constraintMessage) {
        final boolean isNotBlank = StringUtils.isNotBlank(string);
        if (isNotBlank) {
            return Validation.valid(string);
        }
        return Validation.invalid(constraintMessage.apply());
    }

    public static Validation<String, String> validatePattern(
            Pattern pattern,
            String stringToCheck) {
        return validatePattern(pattern, stringToCheck, (string, patternChecked) ->
                format("The string %s does not apply to pattern %s", string, patternChecked.pattern()));
    }

    public static <E> Validation<E, String> validatePattern(
            Pattern pattern,
            String stringToCheck,
            Function2<CharSequence, Pattern, E> constraintMessage) {
        if (!pattern.matcher(stringToCheck).matches()) {
            return Validation.invalid(constraintMessage.apply(stringToCheck, pattern));
        }
        return Validation.valid(stringToCheck);
    }

}
