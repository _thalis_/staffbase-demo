package com.staffbase.scheidewig.bootcamp.quanda.application;

import com.staffbase.scheidewig.bootcamp.quanda.domain.Answer;
import io.vavr.collection.Seq;
import org.springframework.data.repository.CrudRepository;

public interface AnswerRepository extends CrudRepository<Answer, Long> {

    Seq<Answer> findAllByQuestionIdOrderByCreationTimeAsc(String questionId);

}
