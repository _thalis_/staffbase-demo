package com.staffbase.scheidewig.bootcamp.quanda.events;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

import java.time.Instant;

@ToString
@Getter
public class UserRegistered extends ApplicationEvent {

    private final String emailAddress;
    private final String activationLink;
    private final Instant registrationTime;

    public UserRegistered(
            Object source,
            String emailAddress,
            String activationLink,
            Instant registrationTime) {
        super(source);
        this.emailAddress = emailAddress;
        this.activationLink = activationLink;
        this.registrationTime = registrationTime;
    }
}
