package com.staffbase.scheidewig.bootcamp.quanda.application;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@ToString
@Getter
@Setter
public class NewQuestion {

    @NotBlank
    private String userId;
    @NotBlank
    private String title;
    @NotBlank
    private String questionText;

    public NewQuestion(
            @NotBlank String userId,
            @NotBlank String title,
            @NotBlank String questionText) {
        this.userId = userId;
        this.title = title;
        this.questionText = questionText;
    }

    @SuppressWarnings("unused") // Used by Jackson
    public NewQuestion() {
        super();
    }
}
