package com.staffbase.scheidewig.bootcamp.quanda.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "COMMENT", schema = "QUANDA")
public class Comment {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(name = "COMMENT_ID", nullable = false)
    private String commentId;

    @Column(name = "POST_ID", nullable = false)
    private String postId;

    @Column(name = "COMMENT", nullable = false, length = 3000)
    private String comment;

    @Column(name = "COMMENTING_USER")
    private String commentingUser;

    @Column(name = "CREATION_TIME")
    private Instant creationTime;

    public Comment(String commentingUser, String postId, String comment) {
        this.commentId = UUID.randomUUID().toString();
        this.postId = postId;
        this.comment = comment;
        this.commentingUser = commentingUser;
        this.creationTime = Instant.now();
    }

    // JPA constructor.
    protected Comment() {
        super();
    }

    public String getCommentId() {
        return commentId;
    }

    public String getPostId() {
        return postId;
    }

    public String getComment() {
        return comment;
    }

    public String getCommentingUser() {
        return commentingUser;
    }

    public Instant getCreationTime() {
        return creationTime;
    }
}
